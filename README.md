Jimmy Test API Project (Symfony 5.2)
========================

Installing project
------------------

1. clone project

    ```bash
    git clone git@bitbucket.org:Movingarrow/jimmy.git
    ```
2. run composer install

    ```bash
    composer install
    ```
3. provide your credentials for your database and db name in .env

    ```bash
    DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7"
    ```
4. provide your token for very basic auth in .env

    ```bash
    BASIC_AUTH_TOKEN=provide_your_value
    ```
5. create project database

    ```bash
    bin/console d:d:c
    ```
6. create project schema

    ```bash
    bin/console d:s:c
    ```
7. upload project fixtures

    ```bash
    bin/console hautelook:fixtures:load
    ```
8. start web server (for instance symfony build server)

    ```bash
     symfony server:start
    ```
9. to see API documentation navigate to /api

    ```bash
    http://127.0.0.1:8000/api
    ``` 
10. in order to create/update/delete articles, provide token in authorization in header request
    ```bash
    'authorization' => 'Bearer provide_your_value',
    ```  