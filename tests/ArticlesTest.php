<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Project\Domain\Article\Entity\Article;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

/**
 * Class ArticlesTest
 * @package App\Tests
 */
class ArticlesTest extends ApiTestCase
{
    // This trait provided by HautelookAliceBundle will take care of refreshing the database content to a known state before each test
    use RefreshDatabaseTrait;

    /**
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function testGetCollection(): void
    {
        // The client implements Symfony HttpClient's `HttpClientInterface`, and the response `ResponseInterface`
        $response = static::createClient()->request('GET', '/api/articles');

        self::assertResponseIsSuccessful();
        // Asserts that the returned content type is JSON-LD (the default)
        self::assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        // Asserts that the returned JSON is a superset of this one
        self::assertJsonContains([
            '@context' => '/api/contexts/Article',
            '@id' => '/api/articles',
            '@type' => 'hydra:Collection',
            'hydra:totalItems' => 100,
            'hydra:view' => [
                '@id' => '/api/articles?page=1',
                '@type' => 'hydra:PartialCollectionView',
                'hydra:first' => '/api/articles?page=1',
                'hydra:last' => '/api/articles?page=4',
                'hydra:next' => '/api/articles?page=2',
            ],
        ]);

        // Because test fixtures are automatically loaded between each test, you can assert on them
        self::assertCount(30, $response->toArray()['hydra:member']);
        self::assertMatchesResourceCollectionJsonSchema(Article::class);
    }

    /**
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function testCreateArticle(): void
    {
        $response = static::createClient()->request('POST', '/api/articles', [
            'json' => [
                'title' => 'The Handmaid\'s Tale',
                'body' => 'Brilliantly conceived and executed, this powerful evocation of twenty-first century America gives full rein to Margaret Atwood\'s devastating irony, wit and astute perception.'],
            'headers' => [
            'authorization' => 'Bearer ' . 'provide_your_value',
            ]]);

        self::assertResponseStatusCodeSame(201);
        self::assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        self::assertJsonContains([
            '@type' => 'Article',
            'title' => 'The Handmaid\'s Tale',
            'body' => 'Brilliantly conceived and executed, this powerful evocation of twenty-first century America gives full rein to Margaret Atwood\'s devastating irony, wit and astute perception.',
        ]);
        self::assertRegExp('~^/api/articles/\d+$~', $response->toArray()['@id']);
    }

    /**
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function testUpdateBook(): void
    {
        $client = static::createClient();

        $iri = $this->findIriBy(Article::class, ['id' => 55]);

        $client->request('PUT', $iri, [
            'json' => [
                'title' => 'updated title',
                'body' =>  'updated body',
            ],
            'headers' => [
                'authorization' => 'Bearer ' . 'provide_your_value',
            ]]);

        self::assertResponseIsSuccessful();
        self::assertJsonContains([
            '@id' => $iri,
            'id' => 55,
            'title' => 'updated title',
            'body' =>  'updated body',
        ]);
    }

    /**
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function testDeleteArticle(): void
    {
        $client = static::createClient();
        $iri = $this->findIriBy(Article::class, ['id' => 55]);

        $client->request('DELETE', $iri, [
            'headers' => [
                'authorization' => 'Bearer ' . 'provide_your_value',
        ]]);

        self::assertResponseStatusCodeSame(204);
        self::assertNull(
            static::$container->get('doctrine')->getRepository(Article::class)->findOneBy(['id' => 55])
        );
    }
}
