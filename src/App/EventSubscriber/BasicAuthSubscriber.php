<?php
declare(strict_types=1);

namespace App\EventSubscriber;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class BasicAuthSubscriber
 * @package App\EventSubscriber
 */
final class BasicAuthSubscriber implements EventSubscriberInterface
{
    /**
     * @var ParameterBagInterface
     */
    private ParameterBagInterface $parameterBag;

    /**
     * BasicAuthSubscriber constructor.
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    /**
     * @return array|\array[][]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [['basicAuth', 0]],
        ];
    }

    /**
     * @param RequestEvent $event
     */
    public function basicAuth(RequestEvent $event): void
    {
        // only deal with the main request, disregard sub-requests
        if (!$event->isMasterRequest()) {
            return;
        }

        // if request is GET let it through
        if ($event->getRequest()->getMethod() === 'GET') {
            return;
        }

        // if token is valid let it pass
        if ($event->getRequest()->headers->get('authorization') === 'Bearer ' . $this->parameterBag->get('basic_auth')) {
            return;
        }

        $event->setResponse(new JsonResponse(['error' => 'invalid authentication']));
    }
}
