<?php

namespace Project\Domain\Article\Dto;

/**
 * Class ArticleDto
 */
final class ArticleDto
{
    /**
     * @var int
     */
    public int $id;
    /**
     * @var string
     */
    public string $title;
    /**
     * @var string
     */
    public string $body;
    /**
     * @var string
     */
    public string $createdAt;
    /**
     * @var string
     */
    public string $updatedAt;
}