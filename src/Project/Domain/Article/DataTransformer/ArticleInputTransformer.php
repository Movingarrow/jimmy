<?php

namespace Project\Domain\Article\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;
use Project\Domain\Article\Dto\ArticleDto;
use Project\Domain\Article\Entity\Article;

/**
 * Class ArticleInputTransformer
 * @package App\DataTransformer
 */
final class ArticleInputTransformer implements DataTransformerInterface
{
    /** * @var ValidatorInterface */
    private ValidatorInterface $validator;

    /**
     * ArticleInputTransformer constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param ArticleDto $object
     * @param string $to
     * @param array $context
     * @return Article
     */
    public function transform($object, string $to, array $context = []): Article
    {
        $this->validator->validate($object);

        $article = $context['object_to_populate'] ?? new Article();

        $article->setTitle($object->title);
        $article->setBody($object->body);

        return $article;
    }

    /**
     * @param array|object $data
     * @param string $to
     * @param array $context
     * @return bool
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Article) {
            return false;
        }

        return $to === Article::class && ($context['input']['class'] ?? null) !== null;
    }
}
