<?php

namespace Project\Domain\Article\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use Project\Domain\Article\Dto\ArticleDto;
use Project\Domain\Article\Entity\Article;

/**
 * Class ArticleOutputTransformer
 * @package App\DataTransformer
 */
final class ArticleOutputTransformer implements DataTransformerInterface
{
    /**
     * @param Article $object
     * @param string $to
     * @param array $context
     * @return ArticleDto
     */
    public function transform($object, string $to, array $context = []): ArticleDto
    {
        $output = new ArticleDto();
        $output->id = $object->getId();
        $output->title = $object->getTitle();
        $output->body = $object->getBody();
        $output->createdAt = $object->getCreatedAt()->format('Y-m-d H:i:s');
        $output->updatedAt = $object->getUpdatedAt()->format('Y-m-d H:i:s');

        return $output;
    }

    /**
     * @param array|object $data
     * @param string $to
     * @param array $context
     * @return bool
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return $to === ArticleDto::class && $data instanceof Article;
    }
}
